import { Router } from "express";
import cors from "cors";
import parser from "body-parser";
import compression from "compression";

let middlewares = Router();

middlewares.use(cors({ credentials: true, origin: true }));
middlewares.use(parser.urlencoded({ extended: true }));
middlewares.use(parser.json());
middlewares.use(compression());

export default middlewares;