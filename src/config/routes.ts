import { Router, Request, Response } from "express";
import swaggerUi from "swagger-ui-express";
import SearchController from './../app/controllers/search.controller';
import swaggerDocument from "./swagger.json";

let router = Router();

router.get("/", (req: Request, res: Response) => {
    res.send('hello world');
});

router.get("/api/search", (req: Request, res: Response) => {
    const ctrl = new SearchController();
    ctrl.search(req, res);
});

router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));


export = router;