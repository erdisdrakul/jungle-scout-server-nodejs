import http from "http";
import dotenv from 'dotenv';
import express from "express";
import routes from "./config/routes";
import middlewares from "./config/middlewares";

dotenv.config();

const app = express();
app.use(middlewares);
app.use("/", routes);

const { PORT = 3000 } = process.env;
const server = http.createServer(app);

server.listen(PORT, () =>
    console.log(`Server is running http://localhost:${ PORT }...`)
);