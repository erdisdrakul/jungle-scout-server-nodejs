import { Request, Response } from 'express';
import request from 'request';
import cheerio from 'cheerio';
import ProductModel from './../models/product.model';

export default class SearchController {
    async search(req: Request, res: Response) {
        const htmlPage = await this.requestHtmlPage(`${ process.env.AMAZON_URL }/s?k=${ req.query.k }&page=${ req.query.page }`);
        const $ = cheerio.load(htmlPage);
        const values = $('div.s-search-results div[data-asin!=""]');
        
        const products: ProductModel[] = [];
        for (let i = 0; i < values.length; ++i) {
            const el = values[i];

            let stars = this.extractStars(el, $);

            const product: ProductModel = {
                asin: $(el).attr('data-asin'),
                name: $(el).find('h2').text(),
                brand: 'HOMESTARTS',
                price: Number.parseFloat($(el).find('.a-price .a-offscreen').text().substring(1)),
                category: 'Home',
                rank: Math.round(Math.random() * 100),
                monthlySales: Math.round(Math.random() * 1000),
                dailySales: Math.round(Math.random() * 100),
                revenue: Math.round(Math.random() * 5000),
                reviews: parseInt($(el).find('.a-size-small .a-link-normal').text()),
                seller: 'OMZ',
                imageUrl: $(el).find('.s-image').attr('src'),
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                stars: stars
            };
            products.push(product);
        }
        
        res.json(products);
    }

    private async requestHtmlPage(url: string): Promise<string> {
        return new Promise((resolve, reject) => {
            request({url, gzip: true}, (err, res, body) => {
                if (res.statusCode == 200) resolve(body);
                else reject();
            });
        });
    }

    private async setAdditionalProductInfo(url: string, product: ProductModel) {
        const htmlPage = await this.requestHtmlPage(url);
        const $ = cheerio.load(htmlPage);
        product.brand = $('#bylineInfo').text();
        product.seller = product.brand;
    }

    private extractStars(el: CheerioElement, $: CheerioStatic): number {
        let stars = 0;
        const starsEl = $(el).find('.a-size-small span:first-child');
        if (starsEl) {
            const starsLabel = starsEl.attr('aria-label');
            if (starsLabel) {
                stars = Math.round(parseFloat(starsLabel.replace(' out of 5 stars', '')));
            }
        }
        return stars;
    }
}
