"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request_1 = __importDefault(require("request"));
const cheerio_1 = __importDefault(require("cheerio"));
class SearchController {
    search(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const htmlPage = yield this.requestHtmlPage(`${process.env.AMAZON_URL}/s?k=${req.query.k}&page=${req.query.page}`);
            const $ = cheerio_1.default.load(htmlPage);
            const values = $('div.s-search-results div[data-asin!=""]');
            const products = [];
            for (let i = 0; i < values.length; ++i) {
                const el = values[i];
                let stars = this.extractStars(el, $);
                const product = {
                    asin: $(el).attr('data-asin'),
                    name: $(el).find('h2').text(),
                    brand: 'HOMESTARTS',
                    price: Number.parseFloat($(el).find('.a-price .a-offscreen').text().substring(1)),
                    category: 'Home',
                    rank: Math.round(Math.random() * 100),
                    monthlySales: Math.round(Math.random() * 1000),
                    dailySales: Math.round(Math.random() * 100),
                    revenue: Math.round(Math.random() * 5000),
                    reviews: parseInt($(el).find('.a-size-small .a-link-normal').text()),
                    seller: 'OMZ',
                    imageUrl: $(el).find('.s-image').attr('src'),
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                    stars: stars
                };
                products.push(product);
            }
            res.json(products);
        });
    }
    requestHtmlPage(url) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                request_1.default({ url, gzip: true }, (err, res, body) => {
                    if (res.statusCode == 200)
                        resolve(body);
                    else
                        reject();
                });
            });
        });
    }
    setAdditionalProductInfo(url, product) {
        return __awaiter(this, void 0, void 0, function* () {
            const htmlPage = yield this.requestHtmlPage(url);
            const $ = cheerio_1.default.load(htmlPage);
            product.brand = $('#bylineInfo').text();
            product.seller = product.brand;
        });
    }
    extractStars(el, $) {
        let stars = 0;
        const starsEl = $(el).find('.a-size-small span:first-child');
        if (starsEl) {
            const starsLabel = starsEl.attr('aria-label');
            if (starsLabel) {
                stars = Math.round(parseFloat(starsLabel.replace(' out of 5 stars', '')));
            }
        }
        return stars;
    }
}
exports.default = SearchController;
//# sourceMappingURL=search.controller.js.map