"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
const routes_1 = __importDefault(require("./config/routes"));
const middlewares_1 = __importDefault(require("./config/middlewares"));
dotenv_1.default.config();
const app = express_1.default();
app.use(middlewares_1.default);
app.use("/", routes_1.default);
const { PORT = 3000 } = process.env;
const server = http_1.default.createServer(app);
server.listen(PORT, () => console.log(`Server is running http://localhost:${PORT}...`));
//# sourceMappingURL=server.js.map