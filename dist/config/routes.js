"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const express_1 = require("express");
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const search_controller_1 = __importDefault(require("./../app/controllers/search.controller"));
const swagger_json_1 = __importDefault(require("./swagger.json"));
let router = express_1.Router();
router.get("/", (req, res) => {
    res.send('hello world');
});
router.get("/api/search", (req, res) => {
    const ctrl = new search_controller_1.default();
    ctrl.search(req, res);
});
router.use("/api-docs", swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_json_1.default));
module.exports = router;
//# sourceMappingURL=routes.js.map