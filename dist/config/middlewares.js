"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const compression_1 = __importDefault(require("compression"));
let middlewares = express_1.Router();
middlewares.use(cors_1.default({ credentials: true, origin: true }));
middlewares.use(body_parser_1.default.urlencoded({ extended: true }));
middlewares.use(body_parser_1.default.json());
middlewares.use(compression_1.default());
exports.default = middlewares;
//# sourceMappingURL=middlewares.js.map